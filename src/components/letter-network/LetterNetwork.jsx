import React, { useRef } from 'react';
import { useFrame } from '@react-three/fiber';
import { Instances } from '@react-three/drei';
import * as THREE from 'three';
import Letter from './Letter';
import { LETTER_POSITIONS, MATERIAL_CONFIG } from '../../constants/networkConfig';

export default function LetterNetwork({ isLowPower }) {
  const groupRef = useRef();
  const letterKeys = ['S', 'P', 'O1', 'E', 'R', 'I', 'C', 'O2'];

  // Generate connections for each letter
  const generateConnections = (points) => {
    const connections = [];
    for (let i = 0; i < points.length - 1; i++) {
      connections.push([i, i + 1]);
    }
    // Check if shape is closed (first and last points match)
    if (
      points.length > 2 &&
      points[0][0] === points[points.length - 1][0] &&
      points[0][1] === points[points.length - 1][1]
    ) {
      connections.push([points.length - 1, 0]);
    }
    return connections;
  };

  // Subtle rotation animation (disabled on low-power devices)
  useFrame(({ clock }) => {
    if (groupRef.current && !isLowPower) {
      const time = clock.getElapsedTime();
      groupRef.current.rotation.x = Math.sin(time * 0.1) * 0.05 + 0.1;
      groupRef.current.rotation.y = Math.cos(time * 0.1) * 0.05 + 0.2;
    }
  });

  return (
    <group ref={groupRef}>
      <Instances>
        <sphereGeometry args={[0.1, 16, 16]} />
        <meshStandardMaterial {...MATERIAL_CONFIG} />
        {letterKeys.map((key, index) => {
          const points = LETTER_POSITIONS[key];
          const connections = generateConnections(points);
          return (
            <Letter
              key={key}
              points={points}
              connections={connections}
              index={index}
              isLowPower={isLowPower}
            />
          );
        })}
      </Instances>
    </group>
  );
}