import React, { useRef, useState } from 'react';
import { useFrame } from '@react-three/fiber';
import { Instance } from '@react-three/drei';
import * as THREE from 'three';

export default function LetterNode({ basePosition, isHighlighted }) {
  const ref = useRef();
  const [hovered, setHovered] = useState(false);
  const velocity = useRef(new THREE.Vector3());

  useFrame(({ mouse, viewport, clock }) => {
    if (!ref.current) return;
    const time = clock.getElapsedTime();

    // Mouse position in world coordinates
    const mouseX = (mouse.x * viewport.width) / 2;
    const mouseY = (mouse.y * viewport.height) / 2;
    const mousePos = new THREE.Vector3(mouseX, mouseY, 0);

    // Current position
    const currentPos = ref.current.position;

    // Mouse attraction if close
    const distance = currentPos.distanceTo(mousePos);
    if (distance < 1) {
      const force = (0.1 / (distance + 0.1)) * 0.05;
      const direction = mousePos.clone().sub(currentPos).normalize();
      velocity.current.add(direction.multiplyScalar(force));
    }

    // Spring back to base position
    const basePos = new THREE.Vector3(...basePosition);
    const spring = basePos.clone().sub(currentPos).multiplyScalar(0.05);
    velocity.current.add(spring);
    velocity.current.multiplyScalar(0.9); // Damping

    // Update position
    ref.current.position.add(velocity.current);

    // Highlight effects
    const targetScale = (hovered || isHighlighted) ? 1.2 : 1;
    ref.current.scale.lerp(new THREE.Vector3(targetScale, targetScale, targetScale), 0.1);

    if (ref.current.material) {
      ref.current.material.emissiveIntensity =
        (hovered || isHighlighted) ? 0.8 + Math.sin(time * 3) * 0.2 : 0.6;
    }
  });

  return (
    <Instance
      ref={ref}
      position={basePosition}
      onPointerOver={() => setHovered(true)}
      onPointerOut={() => setHovered(false)}
    />
  );
}