import React, { useRef, useMemo } from 'react';
import { useFrame } from '@react-three/fiber';
import LetterNode from './LetterNode';
import Synapse from '../neural-network/Synapse';

export default function Letter({ points, connections, index, isLowPower }) {
  const activeSegmentRef = useRef(0);

  // Generate 3D positions with random z-offsets
  const zOffsets = useMemo(() => points.map(() => (Math.random() - 0.5) * 0.5), [points]);
  const nodes3D = useMemo(
    () => points.map((pos, i) => [...pos, zOffsets[i]]),
    [points, zOffsets]
  );

  // Animate active segment with wave effect across letters
  useFrame(({ clock }) => {
    if (!isLowPower) {
      const time = clock.getElapsedTime();
      const delay = index * 0.2; // Stagger animation by letter
      const t = (time + delay) % 2; // 2-second cycle
      activeSegmentRef.current = Math.floor((t / 2) * connections.length);
    }
  });

  // Determine active nodes for highlighting
  const activeNodes = useMemo(() => {
    const set = new Set();
    if (
      activeSegmentRef.current >= 0 &&
      activeSegmentRef.current < connections.length
    ) {
      const [from, to] = connections[activeSegmentRef.current];
      set.add(from);
      set.add(to);
    }
    return set;
  }, [connections, activeSegmentRef.current]);

  return (
    <>
      {nodes3D.map((pos, i) => (
        <LetterNode
          key={i}
          basePosition={pos}
          isHighlighted={activeNodes.has(i) && !isLowPower}
        />
      ))}
      {connections.map(([from, to], i) => (
        <Synapse
          key={i}
          from={nodes3D[from]}
          to={nodes3D[to]}
          isActive={i === activeSegmentRef.current && !isLowPower}
        />
      ))}
    </>
  );
}