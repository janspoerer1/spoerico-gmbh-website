import React from 'react';

/**
 * Simple loading spinner component for 3D content loading states
 */
export default function LoadingSpinner() {
  return (
    <div className="loading-spinner">
      <div className="spinner"></div>
      <p>Loading visualization...</p>
    </div>
  );
}