// src/components/neural-network/Synapse.jsx
import React, { useRef, useMemo } from 'react';
import { Line } from '@react-three/drei';
import { useFrame } from '@react-three/fiber';
import PropTypes from 'prop-types';

/**
 * Synapse component that visualizes the connection between neurons.
 * 
 * Adjustments:
 * - Removed the traveling dot (energy pulse effect) that animated between nodes.
 * - The connection is now solely represented by the line, which adjusts its opacity,
 *   linewidth, and color based on activation state.
 *
 * @param {Object} props - Component properties.
 * @param {Array} props.from - Starting position [x, y, z].
 * @param {Array} props.to - Ending position [x, y, z].
 * @param {boolean} props.isActive - Whether the synapse is currently activated.
 * @param {number} props.weight - Connection weight (0.1 to 1.0).
 * @param {number} props.activationLevel - Current activation level (0 to 1).
 * @param {boolean} props.hovered - Whether the synapse is hovered.
 */
export default function Synapse({ 
  from, 
  to, 
  isActive = false, 
  weight = 0.5, 
  activationLevel = 0,
  hovered = false
}) {
  const lineRef = useRef();
  
  // Pre-calculate the directional vector for performance, although it's no longer used for pulse animation.
  const vector = useMemo(() => [to[0] - from[0], to[1] - from[1], to[2] - from[2]], [from, to]);
  
  // Calculate color based on weight and activation level.
  const getColorFromWeight = (baseWeight, activeLevel = 0) => {
    const intensity = Math.floor(40 + baseWeight * 215);
    if (activeLevel > 0) {
      const r = Math.min(255, intensity + activeLevel * 80);
      const g = Math.min(255, intensity + activeLevel * 80);
      const b = 255;
      return `rgb(${r}, ${g}, ${b})`;
    }
    return `rgb(${40}, ${40}, ${intensity})`;
  };
  
  const baseColor = useMemo(() => getColorFromWeight(weight), [weight]);
  const activeColor = useMemo(() => getColorFromWeight(weight, activationLevel), [weight, activationLevel]);
  
  // Determine line width based on connection state.
  const getLineWidth = () => {
    if (hovered) return 2;
    if (isActive) return 0.8 + weight * 1.2;
    return 0.5 + weight * 0.5;
  };

  // Update the line material properties based on activation/hover state.
  useFrame(({ clock }) => {
    if (!lineRef.current) return;
    
    const time = clock.getElapsedTime();
    if (lineRef.current.material) {
      if (isActive || hovered) {
        // Create a subtle pulsing effect on the line's opacity.
        const pulse = Math.sin(time * 3) * 0.2 + 0.8;
        lineRef.current.material.opacity = pulse;
        lineRef.current.material.linewidth = getLineWidth();
        if (hovered) {
          lineRef.current.material.color.set("#6ab0ff");
        } else if (isActive) {
          lineRef.current.material.color.set(activeColor);
        }
      } else {
        lineRef.current.material.opacity = 0.1 + weight * 0.2;
        lineRef.current.material.linewidth = getLineWidth();
        lineRef.current.material.color.set(baseColor);
      }
    }
  });

  // Render only the connection line; the traveling dot has been removed.
  return (
    <Line
      ref={lineRef}
      points={[from, to]}
      color={baseColor}
      lineWidth={getLineWidth()}
      transparent
      opacity={0.2 + weight * 0.3}
    />
  );
}

Synapse.propTypes = {
  from: PropTypes.arrayOf(PropTypes.number).isRequired,
  to: PropTypes.arrayOf(PropTypes.number).isRequired,
  isActive: PropTypes.bool,
  weight: PropTypes.number,
  activationLevel: PropTypes.number,
  hovered: PropTypes.bool
};
