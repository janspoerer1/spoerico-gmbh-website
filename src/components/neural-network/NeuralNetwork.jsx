// src/components/neural-network/NeuralNetwork.jsx
import React, { useState, useRef, useEffect } from 'react';
import { useFrame, useThree } from '@react-three/fiber';
import Neuron from './Neuron';
import Synapse from './Synapse';
import { 
  NEURON_POSITIONS, 
  NEURON_CONNECTIONS,
  NETWORK_CONFIG
} from '../../constants/networkConfig';

/**
 * NeuralNetwork component that visualizes forward propagation.
 *
 * Requirements Implemented:
 * - Visualizes forward propagation passes from left to right.
 * - Highlights active connections with visual cues.
 * - Ensures no active nodes remain after a defined period.
 * - Automatically triggers a new forward pass after a delay.
 */
export default function NeuralNetwork({ isLowPower }) {
  const { viewport } = useThree();
  const networkRef = useRef();
  const [activeConnections, setActiveConnections] = useState({});
  const [activeNeurons, setActiveNeurons] = useState({});
  const [hoveredNeuron, setHoveredNeuron] = useState(null);

  // Define network layers (7 neurons per layer)
  const NETWORK_LAYERS = [
    { startIndex: 0, count: 7 },
    { startIndex: 7, count: 7 },
    { startIndex: 14, count: 7 },
    { startIndex: 21, count: 7 },
    { startIndex: 28, count: 7 },
    { startIndex: 35, count: 7 },
    { startIndex: 42, count: 7 },
  ];

  // Configuration constants (with defaults from NETWORK_CONFIG)
  const CONNECTION_ACTIVE_TIME = NETWORK_CONFIG.connectionActiveTime || 300; // in ms
  const ACTIVATION_DELAY = NETWORK_CONFIG.activationDelay || 100; // in ms
  const CYCLE_DELAY = NETWORK_CONFIG.cycleDelay || 4000; // in ms, delay between cycles

  // Ref for static connection weights
  const connectionWeightsRef = useRef(null);

  // Animation state ref to track progress, timers, etc.
  const animationStateRef = useRef({
    isAnimating: false,
    currentInputNeuron: 0,
    lastCycleTime: 0,
    cycleDelay: CYCLE_DELAY / 1000, // convert to seconds for useFrame
    activeConnections: new Set(),
    activeNeurons: new Set(),
    timers: []
  });

  // Initialize connection weights and clear active states on mount
  useEffect(() => {
    if (!connectionWeightsRef.current) {
      const weights = {};
      NEURON_CONNECTIONS.forEach((connection, index) => {
        weights[index] = 0.1 + Math.random() * 0.9; // random weight between 0.1 and 1.0
      });
      connectionWeightsRef.current = weights;
    }

    // Initialize active states for neurons and connections
    const initialActiveConnections = {};
    NEURON_CONNECTIONS.forEach((_, index) => {
      initialActiveConnections[index] = false;
    });
    setActiveConnections(initialActiveConnections);

    const initialActiveNeurons = {};
    NEURON_POSITIONS.forEach((_, index) => {
      initialActiveNeurons[index] = false;
    });
    setActiveNeurons(initialActiveNeurons);

    // Cleanup: clear any pending timers when component unmounts
    return () => {
      animationStateRef.current.timers.forEach(timer => clearTimeout(timer));
    };
  }, []);

  // useFrame loop to check cycle timing and add a subtle rotation effect
  useFrame(({ clock }) => {
    const time = clock.getElapsedTime();
    const state = animationStateRef.current;

    // Start a new forward pass if the cycle delay has passed
    if (!state.isAnimating && time - state.lastCycleTime > state.cycleDelay) {
      resetAllActivations();
      state.isAnimating = true;
      state.currentInputNeuron = 0;
      state.lastCycleTime = time;
      state.activeConnections = new Set();
      state.activeNeurons = new Set();
      state.timers = [];

      // Begin the forward propagation after a short delay
      const timer = setTimeout(() => {
        startForwardPass();
      }, 100);
      state.timers.push(timer);
    }

    // Rotate the network slightly for visual interest
    if (networkRef.current) {
      networkRef.current.rotation.y = Math.sin(time * 0.1) * 0.05;
      networkRef.current.rotation.x = Math.cos(time * 0.1) * 0.03;
    }
  });

  /**
   * Resets all active neurons and connections.
   */
  const resetAllActivations = () => {
    setActiveConnections(prev => {
      const newState = { ...prev };
      Object.keys(newState).forEach(key => newState[key] = false);
      return newState;
    });
    setActiveNeurons(prev => {
      const newState = { ...prev };
      Object.keys(newState).forEach(key => newState[key] = false);
      return newState;
    });
    animationStateRef.current.activeConnections.clear();
    animationStateRef.current.activeNeurons.clear();
  };

  /**
   * Initiates the forward pass starting from the first input neuron.
   */
  const startForwardPass = () => {
    const firstInputNeuron = NETWORK_LAYERS[0].startIndex;
    activateNeuron(firstInputNeuron);
  };

  /**
   * Returns all outgoing connections for a given neuron index.
   */
  const findConnections = (fromNeuronIndex) => {
    return NEURON_CONNECTIONS.map((connection, index) => {
      const from = Array.isArray(connection) ? connection[0] : connection.from;
      if (from === fromNeuronIndex) {
        const to = Array.isArray(connection) ? connection[1] : connection.to;
        return { index, to, weight: connectionWeightsRef.current[index] };
      }
      return null;
    }).filter(Boolean);
  };

  /**
   * Activates a neuron and schedules its deactivation and propagation.
   *
   * @param {number} neuronIndex - Index of the neuron to activate.
   */
  const activateNeuron = (neuronIndex) => {
    const state = animationStateRef.current;
    if (state.activeNeurons.has(neuronIndex)) return; // Prevent double activation

    state.activeNeurons.add(neuronIndex);
    setActiveNeurons(prev => ({ ...prev, [neuronIndex]: true }));

    // Schedule deactivation of this neuron after CONNECTION_ACTIVE_TIME
    const neuronTimer = setTimeout(() => {
      setActiveNeurons(prev => ({ ...prev, [neuronIndex]: false }));
      state.activeNeurons.delete(neuronIndex);
    }, CONNECTION_ACTIVE_TIME);
    state.timers.push(neuronTimer);

    // Activate outgoing connections from this neuron
    const outgoingConnections = findConnections(neuronIndex);
    outgoingConnections.forEach(({ index, to, weight }) => {
      state.activeConnections.add(index);
      setActiveConnections(prev => ({ ...prev, [index]: weight }));
      
      // Schedule deactivation of this connection
      const connectionTimer = setTimeout(() => {
        setActiveConnections(prev => ({ ...prev, [index]: false }));
        state.activeConnections.delete(index);
      }, CONNECTION_ACTIVE_TIME);
      state.timers.push(connectionTimer);

      // Propagate activation to the target neuron with a delay based on connection weight
      const activationSpeed = Math.max(50, ACTIVATION_DELAY * (1.1 - weight));
      const targetTimer = setTimeout(() => {
        activateNeuron(to);
      }, activationSpeed);
      state.timers.push(targetTimer);
    });

    // For input layer neurons, cascade the activation to the next neuron
    const neuronLayer = NETWORK_LAYERS.findIndex(layer => 
      neuronIndex >= layer.startIndex && neuronIndex < layer.startIndex + layer.count
    );
    if (neuronLayer === 0) {
      const nextInputIndex = state.currentInputNeuron + 1;
      if (nextInputIndex < NETWORK_LAYERS[0].count) {
        state.currentInputNeuron = nextInputIndex;
        const cascadeTimer = setTimeout(() => {
          const nextNeuronIndex = NETWORK_LAYERS[0].startIndex + nextInputIndex;
          activateNeuron(nextNeuronIndex);
        }, ACTIVATION_DELAY * 2);
        state.timers.push(cascadeTimer);
      } else if (neuronIndex === NETWORK_LAYERS[0].startIndex + NETWORK_LAYERS[0].count - 1) {
        // When the last input neuron is activated, check for completion after a delay
        const completeTimer = setTimeout(() => {
          checkForwardPassCompletion();
        }, CONNECTION_ACTIVE_TIME * 2);
        state.timers.push(completeTimer);
      }
    } else if (neuronLayer === NETWORK_LAYERS.length - 1) {
      // For output neurons, schedule a completion check
      const completeTimer = setTimeout(() => {
        checkForwardPassCompletion();
      }, CONNECTION_ACTIVE_TIME);
      state.timers.push(completeTimer);
    }
  };

  /**
   * Checks if the forward pass has completed and resets the animation state.
   */
  const checkForwardPassCompletion = () => {
    const state = animationStateRef.current;
    if (state.activeConnections.size === 0 && state.activeNeurons.size === 0) {
      state.isAnimating = false;
    } else {
      const checkTimer = setTimeout(() => {
        checkForwardPassCompletion();
      }, 100);
      state.timers.push(checkTimer);
    }
  };

  // Handlers for neuron hover events to enhance visual feedback on synapses
  const handleNeuronHover = (index) => {
    setHoveredNeuron(index);
  };

  const handleNeuronUnhover = () => {
    setHoveredNeuron(null);
  };

  return (
    <group ref={networkRef}>
      {/* Render all neurons */}
      {NEURON_POSITIONS.map((pos, index) => (
        <Neuron 
          key={`neuron-${index}`} 
          position={pos}
          delay={1 + index * 0.3}
          isActive={activeNeurons[index]}
          onHover={() => handleNeuronHover(index)}
          onUnhover={handleNeuronUnhover}
        />
      ))}

      {/* Render all synapses connecting the neurons */}
      {NEURON_CONNECTIONS.map((connection, index) => {
        const from = Array.isArray(connection) ? NEURON_POSITIONS[connection[0]] : NEURON_POSITIONS[connection.from];
        const to = Array.isArray(connection) ? NEURON_POSITIONS[connection[1]] : NEURON_POSITIONS[connection.to];
        const weight = connectionWeightsRef.current ? connectionWeightsRef.current[index] : 0.5;
        const fromIdx = Array.isArray(connection) ? connection[0] : connection.from;
        const toIdx = Array.isArray(connection) ? connection[1] : connection.to;
        const isActive = activeConnections[index] !== false;
        const activationLevel = isActive ? activeConnections[index] : 0;
        return (
          <Synapse
            key={`synapse-${index}`}
            from={from}
            to={to}
            weight={weight}
            isActive={isActive}
            activationLevel={activationLevel}
            hovered={hoveredNeuron !== null && (fromIdx === hoveredNeuron || toIdx === hoveredNeuron)}
          />
        );
      })}

      {/* Ambient light for improved scene illumination */}
      <pointLight 
        position={[0, 0, 2]} 
        intensity={0.5}
        distance={15}
        color="#4a90e2"
      />
    </group>
  );
}
