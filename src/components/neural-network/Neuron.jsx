// src/components/neural-network/Neuron.jsx
import React, { useRef, useState, useEffect } from 'react';
import { useFrame } from '@react-three/fiber';
import { useSphere } from '@react-three/cannon';
import PropTypes from 'prop-types';
import { MATERIAL_CONFIG } from '../../constants/networkConfig';

/**
 * Enhanced neuron component with activation state
 * @param {Object} props - Component properties
 * @param {Array} props.position - Initial position coordinates [x, y, z]
 * @param {number} props.delay - Animation delay for varied movement
 * @param {boolean} props.isActive - Whether the neuron is currently activated
 * @param {Function} props.onHover - Callback when neuron is hovered
 * @param {Function} props.onUnhover - Callback when neuron hover ends
 */
export default function Neuron({ position, delay = 1, isActive = false, onHover, onUnhover }) {
  const [hovered, setHovered] = useState(false);
  const activationRef = useRef(0);
  
  // Physics-based sphere with optimized settings
  const [physicsRef] = useSphere(() => ({
    mass: 0, // Static, not affected by gravity
    position,
    args: [0.2],
  }));

  // Handle activation state changes
  useEffect(() => {
    if (isActive) {
      activationRef.current = 1.0;
    }
  }, [isActive]);

  // Animation frame handler with performance optimizations
  useFrame(({ mouse, clock }) => {
    if (!physicsRef.current) return;
    
    const time = clock.getElapsedTime();
    
    // Mouse interaction effect - simplified calculations
    const x = (mouse.x * 3) / delay;
    const y = (mouse.y * 3) / delay;
    const z = Math.sin(time * 0.5 + delay) * 0.3; // Reduced frequency
    
    // Enhanced hover wiggle effect
    const wiggleX = hovered ? Math.sin(time * 5) * 0.15 : 0;
    const wiggleY = hovered ? Math.cos(time * 5) * 0.15 : 0;
    
    // Create a smooth target position
    const targetPosition = {
      x: position[0] + x + wiggleX,
      y: position[1] + y + wiggleY,
      z: position[2] + z
    };
    
    // Apply smoother movement based on hover state
    const lerpFactor = hovered ? 0.08 : 0.03;
    physicsRef.current.position.lerp(targetPosition, lerpFactor);
    
    // Update material based on hover and activation state
    if (physicsRef.current.material) {
      // Decay activation over time
      if (activationRef.current > 0) {
        activationRef.current = Math.max(0, activationRef.current - 0.02);
      }
      
      // Base emissive intensity on both hover and activation
      const baseIntensity = hovered ? 0.8 : 0.5;
      const activationBoost = activationRef.current * 0.5; // Up to 0.5 additional intensity
      physicsRef.current.material.emissiveIntensity = baseIntensity + activationBoost;
      
      // Subtle pulse effect on hover
      if (hovered) {
        const pulse = 0.8 + Math.sin(time * 3) * 0.2;
        physicsRef.current.material.emissiveIntensity = pulse + activationBoost;
      }
      
      // Update scale based on activation
      const baseScale = 1.0 + activationRef.current * 0.2; // Up to 20% larger when active
      physicsRef.current.scale.set(baseScale, baseScale, baseScale);
    }
  });

  // Handle hover events
  const handlePointerOver = () => {
    setHovered(true);
    if (onHover) onHover();
  };

  const handlePointerOut = () => {
    setHovered(false);
    if (onUnhover) onUnhover();
  };

  return (
    <mesh
      ref={physicsRef}
      onPointerOver={handlePointerOver}
      onPointerOut={handlePointerOut}
    >
      <sphereGeometry args={[0.3, 32, 32]} />
      <meshStandardMaterial 
        {...MATERIAL_CONFIG} 
        emissiveIntensity={hovered || isActive ? 0.8 : 0.5}
      />
    </mesh>
  );
}

Neuron.propTypes = {
  position: PropTypes.arrayOf(PropTypes.number).isRequired,
  delay: PropTypes.number,
  isActive: PropTypes.bool,
  onHover: PropTypes.func,
  onUnhover: PropTypes.func
};