import { useState, useEffect } from 'react';
import { getDeviceOptimizedSettings, isMobileDevice, isLowPerformanceDevice } from '../utils/deviceDetection';

/**
 * Hook to manage device-specific optimizations
 * @returns {Object} Device optimization settings
 */
export const useDeviceOptimizations = () => {
  const [settings, setSettings] = useState(getDeviceOptimizedSettings());
  
  useEffect(() => {
    // Update settings on window resize
    const handleResize = () => {
      setSettings(getDeviceOptimizedSettings());
    };
    
    window.addEventListener('resize', handleResize);
    
    // Try to detect performance issues during runtime
    const detectPerformanceIssues = () => {
      let lastFrameTime = performance.now();
      let lowPerformanceFrames = 0;
      
      const checkFrame = () => {
        const now = performance.now();
        const frameTime = now - lastFrameTime;
        lastFrameTime = now;
        
        // If frame time is consistently above 50ms (less than 20fps), 
        // we consider it a performance issue
        if (frameTime > 50) {
          lowPerformanceFrames++;
        } else {
          lowPerformanceFrames = Math.max(0, lowPerformanceFrames - 0.5);
        }
        
        // If we detect multiple consecutive slow frames, reduce quality settings
        if (lowPerformanceFrames > 5) {
          setSettings(prev => ({
            ...prev,
            reducedAnimations: true,
            enablePostProcessing: false,
            glowEffects: false,
          }));
          
          // Stop monitoring - we've already reduced quality
          return;
        }
        
        requestAnimationFrame(checkFrame);
      };
      
      // Only monitor performance for non-mobile devices
      // Mobile devices will get optimized settings by default
      if (!isMobileDevice()) {
        requestAnimationFrame(checkFrame);
      }
    };
    
    // Start performance monitoring after a short delay
    const timer = setTimeout(detectPerformanceIssues, 3000);
    
    return () => {
      window.removeEventListener('resize', handleResize);
      clearTimeout(timer);
    };
  }, []);
  
  return settings;
};