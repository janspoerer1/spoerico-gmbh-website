// ./src/constants/networkConfig.js

// networkConfig.js - Update neuron positions to spread them out
export const NEURON_POSITIONS = [
  // Input Layer (Layer 0, 7 neurons)
  [-8, 4, 0], [-8, 2.7, 0], [-8, 1.3, 0], [-8, 0, 0], [-8, -1.3, 0], [-8, -2.7, 0], [-8, -4, 0],
  // Hidden Layer 1 (Layer 1, 7 neurons)
  [-5.3, 4, 0], [-5.3, 2.7, 0], [-5.3, 1.3, 0], [-5.3, 0, 0], [-5.3, -1.3, 0], [-5.3, -2.7, 0], [-5.3, -4, 0],
  // Hidden Layer 2 (Layer 2, 7 neurons)
  [-2.7, 4, 0], [-2.7, 2.7, 0], [-2.7, 1.3, 0], [-2.7, 0, 0], [-2.7, -1.3, 0], [-2.7, -2.7, 0], [-2.7, -4, 0],
  // Hidden Layer 3 (Layer 3, 7 neurons)
  [0, 4, 0], [0, 2.7, 0], [0, 1.3, 0], [0, 0, 0], [0, -1.3, 0], [0, -2.7, 0], [0, -4, 0],
  // Hidden Layer 4 (Layer 4, 7 neurons)
  [2.7, 4, 0], [2.7, 2.7, 0], [2.7, 1.3, 0], [2.7, 0, 0], [2.7, -1.3, 0], [2.7, -2.7, 0], [2.7, -4, 0],
  // Hidden Layer 5 (Layer 5, 7 neurons)
  [5.3, 4, 0], [5.3, 2.7, 0], [5.3, 1.3, 0], [5.3, 0, 0], [5.3, -1.3, 0], [5.3, -2.7, 0], [5.3, -4, 0],
  // Output Layer (Layer 6, 7 neurons)
  [8, 4, 0], [8, 2.7, 0], [8, 1.3, 0], [8, 0, 0], [8, -1.3, 0], [8, -2.7, 0], [8, -4, 0],
];

// Define connections between consecutive layers (fully connected)
export const NEURON_CONNECTIONS = (() => {
  const connections = [];
  for (let layer = 0; layer < 6; layer++) { // Connect layers 0->1, 1->2, ..., 5->6
    const fromStart = layer * 7;
    const toStart = (layer + 1) * 7;
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 7; j++) {
        connections.push([fromStart + i, toStart + j]);
      }
    }
  }
  return connections;
})();

export const LETTER_POSITIONS = {
  // S: Creates an S curve with 7 points
  S: [
    [-4.0, -0.5],   // Bottom left
    [-3.5, -0.5], // Bottom right
    [-3.5, 0.25], // Middle right
    [-4.0, 0.25],   // Middle left
    [-4.0, 1],      // Top left
    [-3.5, 1],    // Top right
  ],
  // P: Creates a P shape with 5 points
  P: [
    [-2.8, -0.5], // Bottom
    [-2.8, 1],    // Vertical line up
    [-2.3, 1],    // Top right
    [-2.3, 0.25], // Bottom of loop
    [-2.8, 0.25], // Connect back to vertical
  ],

  // O1: First O with 5 points making a circle
  O1: [
    [-1.8, 1],    // Top left
    [-1.3, 1],    // Top right
    [-1.3, -0.5], // Bottom right
    [-1.8, -0.5], // Bottom left
    [-1.8, 1],    // Back to top left to close
  ],

  // E: Creates an E shape with 7 points
  E: [
    [-0.8, -0.5],  // 1: Bottom left (start)
    [-0.8, 1],     // 2: Top left (up)
    [-0.3, 1],     // 3: Top right (right)
    [-0.8, 1],     // 4: Back to top left (left)
    [-0.8, 0.25],  // 5: Middle left (down)
    [-0.3, 0.25],  // 6: Middle right (right)
    [-0.8, 0.25],  // 7: Back to middle left (left)
    [-0.8, -0.5],  // 8: Bottom left (down)
    [-0.3, -0.5],  // 9: Bottom right (right)
  ],

  // R: Creates an R shape with 6 points
  R: [
    [0.2, -0.5],  // Bottom left
    [0.2, 1],     // Vertical line up
    [0.7, 1],     // Top right
    [0.7, 0.25],  // Bottom of loop
    [0.2, 0.25],  // Connect back to vertical
    [0.7, -0.5],  // Diagonal leg
  ],

  // I: Simple vertical line with 2 points
  I: [
    [1.2, -0.5],  // Bottom
    [1.2, 1],     // Top
  ],

  // C: Creates a C shape with 5 points
  C: [
    [2.2, 0.7],     // Top right
    [2.2, 1],     // Top right
    [1.7, 1],     // Top left
    [1.7, -0.5],  // Bottom left
    [2.2, -0.5],  // Bottom right
    [2.2, -0.2],  // Right indent (to create C shape)
  ],

  // O2: Second O with 5 points making a circle
  O2: [
    [2.7, 1],     // Top left
    [3.2, 1],     // Top right
    [3.2, -0.5],  // Bottom right
    [2.7, -0.5],  // Bottom left
    [2.7, 1],     // Back to top left to close
  ],
};

// Enhanced visual settings for a more appealing look
export const MATERIAL_CONFIG = {
  color: "#4a90e2",           // Main blue color
  emissive: "#2a6ac2",        // Glow color
  emissiveIntensity: 0.6,     // Increased glow intensity
  roughness: 0.1,             // More reflective surface
  metalness: 0.9,             // More metallic look
  transparent: true,          // Enable transparency
  opacity: 0.9,               // Slight transparency for better light effects
};


export const NETWORK_CONFIG = {
  nodeSize: 0.12,             // Slightly larger nodes
  letterScale: 1.8,           // Increased letter scale for visibility
  letterPosition: [0, 0.5, 0],// Adjusted position
};