/**
 * Utility functions for device detection and performance optimization
 */

/**
 * Detects if the current device is a mobile device
 * @returns {boolean} True if the device is mobile
 */
export const isMobileDevice = () => {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || 
           window.innerWidth < 768;
  };
  
  /**
   * Detects if the device has low memory or CPU (based on hardware concurrency)
   * @returns {boolean} True if the device likely has limited resources
   */
  export const isLowPerformanceDevice = () => {
    // Check for hardware concurrency - fewer than 4 threads indicates a potential low-end device
    const hasLowConcurrency = navigator.hardwareConcurrency && navigator.hardwareConcurrency < 4;
    
    // Check for device memory (only supported in some browsers)
    const hasLowMemory = navigator.deviceMemory && navigator.deviceMemory < 4;
    
    // Check if it's a mobile device
    const isMobile = isMobileDevice();
    
    // Consider it low performance if any condition is true
    return hasLowConcurrency || hasLowMemory || isMobile;
  };
  
  /**
   * Returns configuration object with quality settings based on device capabilities
   * @returns {Object} Configuration object with quality settings
   */
  export const getDeviceOptimizedSettings = () => {
    const isLowPerformance = isLowPerformanceDevice();
    const isMobile = isMobileDevice();
    
    return {
      // Three.js specific optimizations
      dpr: isLowPerformance ? 1 : [1, window.devicePixelRatio > 2 ? 2 : window.devicePixelRatio],
      shadowMapSize: isLowPerformance ? 1024 : 2048,
      
      // Geometry detail
      sphereDetail: isLowPerformance ? 16 : 32,
      
      // Animation optimizations
      animationFPS: isLowPerformance ? 30 : 60,
      enablePostProcessing: !isLowPerformance,
      
      // UI optimizations
      showFullNetworks: !isMobile,
      reducedAnimations: isLowPerformance,
      
      // Visual effects
      glowEffects: !isLowPerformance,
      reflections: !isLowPerformance,
      
      // 3D scenes
      renderNeuralNetwork: true, // Always render, but with different quality
      renderLetterNetwork: !isLowPerformance || window.innerWidth > 600, // Skip on very small devices
    };
  };