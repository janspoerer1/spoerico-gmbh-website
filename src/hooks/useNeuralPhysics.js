import { useSphere } from '@react-three/cannon';
import { useFrame } from '@react-three/fiber';
import { useRef } from 'react';

import PropTypes from 'prop-types';

useNeuralPhysics.propTypes = {
  position: PropTypes.arrayOf(PropTypes.number).isRequired,
  delay: PropTypes.number
};

export function useNeuralPhysics(position, delay = 1) {
  const [ref] = useSphere(() => ({
    mass: 1,
    position,
    args: [0.2],
  }));

  const isHovered = useRef(false);

  useFrame(({ mouse, clock }) => {
    if (!ref.current) return;
    
    const x = (mouse.x * 5) / delay;
    const y = (mouse.y * 5) / delay;
    const z = Math.sin(clock.getElapsedTime() + delay) * 0.5;

    const wiggleX = isHovered.current ? Math.sin(clock.getElapsedTime() * 5) * 0.2 : 0;
    const wiggleY = isHovered.current ? Math.cos(clock.getElapsedTime() * 5) * 0.2 : 0;


    ref.current.position.lerp({ 
      x: position[0] + x + wiggleX,
      y: position[1] + y + wiggleY,
      z: position[2] + z
    }, 0.03);
  });

  return ref;
}